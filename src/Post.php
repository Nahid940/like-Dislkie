<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 11/12/2017
 * Time: 12:04 AM
 */

namespace App;


class Post extends DB
{


    public function getAllPost(){

        $sql="select * from post";
        $stmt=$this->dbCon()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function addLikeOrDislike($postid,$user_id){
        $sql="select * from user_like where postid=$postid and user_id=$user_id";
        $stmt=$this->dbCon()->prepare($sql);
        $stmt->execute();
        if($stmt->rowCount()==0){
            $sql="insert into user_like (postid,user_id) VALUES ('$postid','$user_id')";
            $stmt=$this->dbCon()->prepare($sql);
            $stmt->execute();
            return 1;
        }
    }

    public function Dislike($postid,$user_id){
        $sql="delete from user_like where postid=$postid and user_id=$user_id";
        $stmt=$this->dbCon()->prepare($sql);
        $stmt->execute();
    }


    public function checkLike($postid,$user_id){
        $sql="select * from user_like where postid=$postid and user_id=$user_id";
        $stmt=$this->dbCon()->prepare($sql);
        $stmt->execute();
        if($stmt->rowCount()==1){
            return $stmt->fetch(\PDO::FETCH_ASSOC);
        }
    }

}