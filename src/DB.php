<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 11/12/2017
 * Time: 12:05 AM
 */

namespace App;


class DB
{

    protected $con='';

    public function dbCon(){

        $this->con=new \PDO('mysql:host=localhost;dbname=post',"root","");
        $this->con->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);
        return $this->con;

    }

}