
<?php

include_once ('vendor/autoload.php');
$post=new \App\Post();


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>Document</title>
</head>
<body>


<?php
foreach ($post->getAllPost() as $p){
    $r=$post->checkLike(1,$p['postid']);
?>
    <table>
        <tr>
            <?php if($r==0){?>

            <td><a href="" id="like" data-id="<?php echo $p['postid']?>"><i class="fa fa-thumbs-up" aria-hidden="true" ></i></a></td>
            <?php }
                else{
            ?>
            <td><a href="" id="dislike"  data-id="<?php echo $p['postid']?>"><i class="fa fa-thumbs-down" aria-hidden="true"></i></a></td>
            <?php }?>
            <td><?php echo $p['post']?></td>
        </tr>
    </table>
<?php }?>

<script>
    $(document).on("click","#like",function(){
        var id=$(this);

        $.ajax({

            type:'post',
            url:'operation.php',
            data:{
                content:'like',
                postid:(id.data('id'))
            },
            success:function (data) {
                alert(data);
            }
        });

    });

    $(document).on("click","#dislike",function(){
        var id=$(this);

        $.ajax({

            type:'post',
            url:'operation.php',
            data:{
                content:'dislike',
                postid:(id.data('id'))
            },
            success:function (data) {
                alert(data);
            }
        });


    });

</script>

</body>
</html>